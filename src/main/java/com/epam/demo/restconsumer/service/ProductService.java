package com.epam.demo.restconsumer.service;

import com.epam.demo.restconsumer.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

@Service
public class ProductService {
    private RestTemplate restTemplate;

    private String baseUrl = "http://localhost:8080";

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Product[] searchProduct(String name) {
        return restTemplate.getForObject(baseUrl + "/products?q={name}", Product[].class, name);
    }
}
