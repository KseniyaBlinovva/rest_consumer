package com.epam.demo.restconsumer.controllers;

import com.epam.demo.restconsumer.domain.Product;
import com.epam.demo.restconsumer.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/search")
    public Product[] searchProduct(@RequestParam String query){
        return productService.searchProduct(query);
    }
}
